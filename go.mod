module gitlab.com/w1ref1re/ledcore2

go 1.14

require (
	github.com/apex/log v1.1.0
	github.com/golangci/golangci-lint v1.27.0
	github.com/gramework/gramework v1.7.0
	github.com/prometheus/common v0.4.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	gitlab.com/w1ref1re/ledcore v0.0.0-20200510113122-0f32874511a5
	go.uber.org/zap v1.15.0
	honnef.co/go/tools v0.0.1-2020.1.3
)
