// strip is the interface between the handler logic and the arduino
package strip

import (
	"gitlab.com/w1ref1re/ledcore2/models"
	serial "gitlab.com/w1ref1re/ledcore2/serial"
)

// Clear clears the strip
func Clear() (int, error) {
	return serial.SendCommand('c')
}

// Fill fills the strip
// set @count to 0 to fill the whole strip
func Fill(color models.Value, start, count uint32) (int, error) {
	return serial.SendCommand('f', color.AsUint32(), start, count)
}

// SerPixelColor sets the color of one specific neopixel
func SetPixelColor(n uint32, color models.Value) (int, error) {
	return serial.SendCommand('p', n, color.AsUint32())
}


// SetBrightness sets the brightness of the strip
func SetBrightness(brightness uint32) (int, error) {
	return serial.SendCommand('b', brightness)
}
