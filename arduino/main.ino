#include <Adafruit_NeoPixel.h>

#define LED_PIN    6
#define LED_COUNT 147

#define PARAM_COUNT 3

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_BRG + NEO_KHZ800);

typedef union _uvalue {
        char b[4];
        uint32_t v;
} uvalue;

typedef struct _Instruction {
        char command;
        uvalue params[PARAM_COUNT];

} Instruction;


Instruction *readInstruction() {
        // check if needed data is provided
        if (Serial.available() != (1 + 4*PARAM_COUNT)) {
                return NULL;
        }

        // init Instruction
        Instruction *instr = (Instruction *) malloc(sizeof(Instruction));
        if (instr == NULL) {
                return NULL;
        }

        // init uvalue
        uvalue value;

        // read command
        (*instr).command = Serial.read();

        // read params
        for (int i=0; i<PARAM_COUNT; i++) {
                Serial.readBytes((*instr).params[i].b, 4);
        }

        return instr;
}

void printInstruction(Instruction* instr) {
        Serial.write((*instr).command);
        Serial.write('\n');

        char message[11];
        for (int i=0; i<PARAM_COUNT; i++) {
                snprintf(message, sizeof(message), "%lu", (unsigned long) (*instr).params[i].v);
                Serial.write(message);
                Serial.write(0x20);

                snprintf(message, sizeof(message), "%08x", (*instr).params[i].v);
                Serial.write(message);
                Serial.write(0x0a);
        }
}

void setup() {
        Serial.begin(9600);

        strip.begin();
        strip.show();

        // blink red for 100ms to check if everything is working
        strip.fill(strip.Color(255, 0, 0));
        strip.show();
        delay(100);
        strip.clear();
        strip.show();
}

void loop() {

        // read the instruction
        Instruction *instr = readInstruction();
        if (instr == NULL) {
                return;
        }

        // TODO: Implement 'action successfull' check (server and arduino)

        switch ((*instr).command) {
                // fill 'f'
                case 0x66:
                        strip.clear();
                        strip.show();
                        if ((*instr).params[2].v == 0) {
                                strip.fill((*instr).params[0].v, (*instr).params[1].v, LED_COUNT);
                        }
                        strip.fill((*instr).params[0].v, (*instr).params[1].v, (*instr).params[2].v);
                        strip.show();
                        break;
                // clear 'c'
                case 0x63:
                        strip.clear();
                        strip.show();
                        break;
                // setPixelColor 'p'
                case 0x70:
                        strip.setPixelColor((*instr).params[0].v, (*instr).params[1].v);
                        strip.show();
                        break;
                // setBrightness 'b'
                case 0x62:
                        strip.setBrightness((*instr).params[0].v);
                        strip.show();
                        break;
        }

        free(instr);

        // delay to prevent the arduino from breaking
        delay(15);
}
