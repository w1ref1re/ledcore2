package config

// Config is the struct containing the config data
type Config struct {
	// The address and port the server is going to listen on
	ServerAddress, ServerPort string

	// The base path of the api and the command endpoints
	ApiBase, CommandBase string

	// Name of the arduino serial connection e.g. "/dev/ttyACM0"
	SerialName string

	// Baud and read timeout of the arduino serial connection
	SerialBaud, SerialReadTimeout int
}

func NewDefaultConfig(address, port string) *Config {
	return &Config{
		ServerAddress:     address,
		ServerPort:        port,
		ApiBase:           "/api/v1",
		CommandBase:       "/command",
		SerialName:        "/dev/ttyACM0",
		SerialBaud:        9600,
		SerialReadTimeout: 10,
	}
}
