package main

import (
	"time"

	// "fmt"

	"gitlab.com/w1ref1re/ledcore2/internal/config"
	"gitlab.com/w1ref1re/ledcore2/models"
	"gitlab.com/w1ref1re/ledcore2/strip"
)

func blink(ctx *models.HandlerContext, params map[string]*models.Value) (interface{}, int) {
	ctx.Log.Info("Blinking...")
	freq := params["freq"].AsUint32()
	if freq < 34 {
		freq = 34
	}

	for i := uint32(0); i < params["len"].AsUint32(); i++ {
		strip.Fill(*(params["color"]), params["start"].AsUint32(), params["count"].AsUint32())
		time.Sleep(time.Duration(freq) * time.Millisecond)
		strip.Clear()
		time.Sleep(time.Duration(freq) * time.Millisecond)
	}

	return map[string]string{"status": "finished"}, 200
}

func main() {
	server := NewServer(config.NewDefaultConfig("192.168.178.39", "8080"))
	server.AddCommandFunction("/blink", []string{"color", "len", "start", "count", "freq"}, blink)
	server.Start()
}
