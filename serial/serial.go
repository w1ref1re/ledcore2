package serial

import (
	"encoding/binary"
	"time"

	"github.com/tarm/serial"

	"gitlab.com/w1ref1re/ledcore2/internal/config"
)

func Connect(conf *config.Config) error {
	var err error

	s, err = serial.OpenPort(&serial.Config{
		Name:        conf.SerialName,
		Baud:        conf.SerialBaud,
		ReadTimeout: time.Duration(conf.SerialReadTimeout) * time.Second,
	})

	if err != nil {
		return err
	}

	time.Sleep(2 * time.Second)

	return nil
}

func Close() {
	s.Close()
}

func SendCommand(cmd byte, values ...uint32) (int, error) {
	// init buffer
	buf := make([]byte, 4)

	for len(values) < 3 {
		values = append(values, 0)
	}

	// add command char
	result := []byte{cmd}

	// append values
	for _, v := range values {
		binary.LittleEndian.PutUint32(buf, v)
		result = append(result, buf...)
	}

	return s.Write(result)
}
