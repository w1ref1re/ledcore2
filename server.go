package main

import (
	"fmt"

	"github.com/gramework/gramework"
	"gitlab.com/w1ref1re/ledcore2/apiV1"
	"gitlab.com/w1ref1re/ledcore2/internal/config"
	"gitlab.com/w1ref1re/ledcore2/internal/log"
	"gitlab.com/w1ref1re/ledcore2/models"
	"gitlab.com/w1ref1re/ledcore2/serial"
	"go.uber.org/zap"
)

// Server is the server struct
type Server struct {
	// Conf holds the server config
	Conf *config.Config
	// CommandHandlers holds the command handler functions
	CommandHandlers []*models.HandlerFunc
	// App is the Gramework App
	App *gramework.App
	// Log is the server's logger
	Log *zap.Logger
}

// NewServer creates a new Server from a Config
func NewServer(conf *config.Config) *Server {
	return &Server{
		Conf:            conf,
		CommandHandlers: make([]*models.HandlerFunc, 0),
		App:             gramework.New(),
		Log:             log.NewLogger(),
	}
}

// AddCommandFunction adds a command function to the server
// it will be accessible at Conf.ApiBase/Conf.CommandBase/<enspoint>
// and must be added **before** Server.Start is called
func (s *Server) AddCommandFunction(endpoint string, nparams []string,
	f func(*models.HandlerContext, map[string]*models.Value) (interface{}, int)) {
	s.CommandHandlers = append(s.CommandHandlers, models.NewHandlerFunc(endpoint, s.Conf.CommandBase, nparams, &models.HandlerContext{Log: s.Log, Conf: s.Conf}, f))
}

// AddHandlerFunction registers a handler function
func (s *Server) AddHandlerFunction(endpoint, method string, nparams []string,
	f func(*models.HandlerContext, map[string]*models.Value) (interface{}, int)) {
	handlerFunc := models.NewHandlerFunc(endpoint,
		"",
		nparams,
		&models.HandlerContext{Log: s.Log, Conf: s.Conf},
		f,
	)

	s.App.Handle(method, s.Conf.ApiBase+endpoint, handlerFunc.Run)
}

// Start starts the server
func (s *Server) Start() {
	// register basic functions
	s.Log.Info("Registering basic functions")
	s.AddHandlerFunction("/fill", "POST", []string{"color", "start", "count"},
		apiV1.FillHandler)
	s.AddHandlerFunction("/clear", "POST", []string{},
		apiV1.ClearHandler)
	s.AddHandlerFunction("/set", "POST", []string{"color", "n"},
		apiV1.SetHandler)

	// register command functions
	s.Log.Info("Registering command functions")
	s.registerCommandFunctions()

	// connect to arduino
	s.Log.Info("Connecting to arduino")
	if err := serial.Connect(s.Conf); err != nil {
		s.Log.Fatal("Unable to connect to arduino", zap.Error(err))
	}

	s.Log.Info("Led server started successfully!")
	s.App.ListenAndServe(s.Conf.ServerAddress + ":" + s.Conf.ServerPort)
}

// registerCommandFunctions registers all command functions
func (s *Server) registerCommandFunctions() {
	for _, v := range s.CommandHandlers {
		s.App.POST(s.Conf.ApiBase+v.Base+v.Endpoint, v.Run)
		s.Log.Info("Registered command handler", zap.String("HandlerFunc", fmt.Sprintf("%+v", *v)))
	}
}
