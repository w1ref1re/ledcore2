package apiV1

import (
	"gitlab.com/w1ref1re/ledcore2/models"
	"gitlab.com/w1ref1re/ledcore2/strip"
	"go.uber.org/zap"
)

// FillHandler fills the strip
func FillHandler(ctx *models.HandlerContext, params map[string]*models.Value) (interface{}, int) {
	ctx.Log.Info("Filling strip")
	n, err := strip.Fill(*(params["color"]), params["start"].AsUint32(), params["count"].AsUint32())
	if err != nil {
		ctx.Log.Error("Unable to fill strip", zap.Error(err))
		return map[string]string{
			"status": "internal server error",
		}, 500
	}

	return map[string]interface{}{
		"status":        "success",
		"bytes_written": n,
		"color":         params["color"].AsUint32(),
		"action":        "fill",
	}, 200
}

func ClearHandler(ctx *models.HandlerContext, params map[string]*models.Value) (interface{}, int) {
	ctx.Log.Info("Clearing strip")
	n, err := strip.Clear()
	if err != nil {
		ctx.Log.Error("Unable to clear strip", zap.Error(err))
		return map[string]string{
			"status": "internal server error",
		}, 500
	}

	return map[string]interface{}{
		"status":        "success",
		"bytes_written": n,
		"color":         params["color"].String,
		"action":        "fill",
	}, 200
}

func SetHandler(ctx *models.HandlerContext, params map[string]*models.Value) (interface{}, int) {
	ctx.Log.Info("Setting strip pixel")
	n, err := strip.SetPixelColor(params["n"].AsUint32(), *(params["color"]))
	if err != nil {
		ctx.Log.Error("Unable to set strip pixel", zap.Error(err))
		return map[string]string{
			"status": "internal server error",
		}, 500
	}

	return map[string]interface{}{
		"status":        "success",
		"bytes_written": n,
		"color":         params["color"].String,
		"action":        "fill",
	}, 200
}
