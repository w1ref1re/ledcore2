package models

import (
	"bytes"
	"encoding/json"
	"fmt"

	"github.com/gramework/gramework"
	"go.uber.org/zap"
)

// HandlerFunc represents a handler function
// @Func:
// *HandlerContext holds important data like the config and the log struct
// map[string]*Value holds the params
// it returns an interface, which will be sent as response
// and an status code as int value
// supported status codes:
// * 200
// * 500
// * 400
// TODO: research if there is a possibility use all status codes
type HandlerFunc struct {
	Endpoint, Base string
	Params         []string
	Ctx            *HandlerContext
	Func           func(*HandlerContext, map[string]*Value) (interface{}, int)
}

// NewHandlerFunc creates a new HandlerFunc struct
func NewHandlerFunc(endpoint, base string, nparams []string, ctx *HandlerContext, f func(*HandlerContext, map[string]*Value) (interface{}, int)) *HandlerFunc {
	return &HandlerFunc{
		Endpoint: endpoint,
		Base:     base,
		Params:   nparams,
		Ctx:      ctx,
		Func:     f,
	}
}

// Run is the function used as handler function in the gramework Handler
func (h *HandlerFunc) Run(gctx *gramework.Context) interface{} {
	h.Ctx.Log.Info("Running command") //, zap.Reflect("body", rawbody))

	// decode Body
	rawbody := make(map[string]string, 0)
	err := json.NewDecoder(bytes.NewReader(gctx.PostBody())).Decode(&rawbody)
	if err != nil {
		h.Ctx.Log.Error("Unable to decode request body", zap.Error(err))

		gctx.BadRequest()
		return map[string]string{
			"status": "invalid request",
		}
	}
	h.Ctx.Log.Info("Getting params")
	// decode values
	params, err := GetValues(rawbody)
	if err != nil {
		h.Ctx.Log.Error("Unable to decode values", zap.Error(err))
	}
	h.Ctx.Log.Info("Checking params")
	// check if needed params are provided
	if !containsKeys(params, h.Params) {
		h.Ctx.Log.Error("Body is missing some params")

		gctx.BadRequest()
		return map[string]string{
			"status": "invalid request",
		}

	}
	h.Ctx.Log.Info("Running handler fucntion", zap.String("HandlerFunc", fmt.Sprintf("%+v", *h)))
	// TODO: pass important config data, like PIXELCOUNT to function

	// run function
	data, status := h.Func(h.Ctx, params)
	switch status {
	case 200:
		return data
	case 500:
		gctx.Err500()
		return data
	case 400:
		gctx.BadRequest()
		return data

	default:
		return data
	}
}

// containsKeys checks if a map contains the needed keys
func containsKeys(m map[string]*Value, keys []string) bool {
	for _, v := range keys {
		if _, ok := m[v]; !ok {
			return false
		}
	}

	return true
}
