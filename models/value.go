package models

import (
	"encoding/binary"
	"encoding/hex"
	"errors"
)

var (
	ErrInvalidHexLength = errors.New("Invalid length of hex string")
)

// Value represents a param value
type Value struct {
	Number [4]uint8
	String string
}

// NewValue creates a new Value from a string
// Value.String will always be set with @s
// Value.Number will only be set if len(@s) <= 8 and it can be interpreted
// as a 4byte hex string
// if you want the value to be interpreted as string use
// 'NewValueFromString'
func NewValue(s string) (*Value, error) {
	// try creating the value from hex
	val, err := NewValueFromHex([]byte(s))

	// if the hex string is too long,
	// interpret as string
	if err == ErrInvalidHexLength {
		return NewValueFromString(s)
	}

	// some other error occured
	if err != nil {
		return nil, err
	}

	// everything went fine
	return val, nil
}

func NewValueFromHex(s []byte) (*Value, error) {
	if len(s) > 8 {
		return nil, ErrInvalidHexLength
	}

	val := Value{String: string(s)}

	// make s 8 char long
	for len(s) < 8 {
		s = append([]byte{'0'}, s...)
	}

	// decode s
	arr := make([]byte, 4)
	if _, err := hex.Decode(arr, s); err != nil {
		return nil, err
	}

	val.Number = [4]uint8{arr[0], arr[1], arr[2], arr[3]}
	return &val, nil
}

// NewValueFromString creates a new Value and interprets the given string
// **always** as a string
func NewValueFromString(s string) (*Value, error) {
	return &Value{String: s}, nil
}

// GetValues is used to get a [string]*Value map from a raw body
// if an error occurs while processing the final map, processed until the error
// occured, and the error will be returned
func GetValues(rawparams map[string]string) (map[string]*Value, error) {
	// return if rawparams is empty
	if len(rawparams) == 0 {
		return map[string]*Value{}, nil
	}

	params := make(map[string]*Value)
	for k, v := range rawparams {
		val, err := NewValue(v)
		if err != nil {
			return params, err
		}

		params[k] = val
	}

	return params, nil
}

func (v *Value) AsUint32() uint32 {
	return binary.BigEndian.Uint32(v.Number[:])
}
