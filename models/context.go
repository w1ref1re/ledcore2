package models

import (
	"go.uber.org/zap"
	"gitlab.com/w1ref1re/ledcore2/internal/config"
)

// HandlerContext is passed to the handler functions and contains important information
// like the logger object and the config
type HandlerContext struct {
	Log  *zap.Logger
	Conf *config.Config
}
